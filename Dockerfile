FROM ubuntu
RUN mkdir /scripts
ADD build-patcher /scripts/
ADD build-template.yaml /scripts/
ADD config.ini /scripts/
ADD README.md /scripts/
RUN chmod a+x /scripts/build-patcher
